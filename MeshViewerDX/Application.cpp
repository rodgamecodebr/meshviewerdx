#include "Application.h"

Application::Application()
{
	std::cout << "Application started..." << std::endl;
}

Application::~Application()
{
	std::cout << "Application finalized..." << std::endl;
}

bool Application::Init()
{
	bool WINDOW_INIT = Window::Init();
	bool RENDERER_INIT = Renderer::Init();
	
	return WINDOW_INIT && RENDERER_INIT;
}

void Application::Loop()
{
	Window::Loop();
}

HINSTANCE Application::GetApplicationWindow()
{
	return Window::GetInstance()->GetHInstance();
}
#pragma once

#ifndef MESHVIEWER_H_
#define MESHVIEWER_H_

#include "Application.h"
#include "Window.h"

int main()
{
	if (Application::Init())
	{
		std::cout << "Application initiated successfully!" << std::endl;

		Application::Loop();
	}

	return 0;
}
  
#endif MESHVIEWER_H_
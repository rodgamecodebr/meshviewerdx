#include "Renderer.h"

Renderer* Renderer::Singleton = 0;

D3DXVECTOR3 position;
D3DXVECTOR3 up;
D3DXVECTOR3 target;

D3DXMATRIX teapot;
D3DXMATRIX view;
D3DXMATRIX proj;

struct Vertex
{
	Vertex() {}
	Vertex(float x, float y, float z)
	{
		_x = x;   _y = y;   _z = z;
	}

	float _x, _y, _z;

	static const DWORD FVF;
};
const DWORD Vertex::FVF = D3DFVF_XYZ;

Renderer::Renderer()
{
	D3D9 = 0;
	Device = 0;
	DeviceType = D3DDEVTYPE_HAL;
	Msg = {};
	VertexBuffer = 0;
	IndexBuffer = 0;
	RenderWindow = Window::GetHwnd();
}

Renderer::~Renderer()
{
	MeshViewerUtilities::Release<ID3DXMesh*>(Teapot);
	MeshViewerUtilities::Release<ID3DXMesh*>(DefaultMesh);
}

bool Renderer::CreateDevice()
{
	HRESULT hr = 0;

	D3D9 = Direct3DCreate9(D3D_SDK_VERSION);

	if (!D3D9)
	{
		::MessageBoxA(0, "Direct3DCreate9() Failed.", 0, 0);
		return false;
	}

	D3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, DeviceType, &Caps);

	int vp = 0;

	if (Caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		vp = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		vp = D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	// Fill out device capabilities
	PresentParameters.BackBufferWidth = Width;
	PresentParameters.BackBufferHeight = Height;
	PresentParameters.BackBufferFormat = D3DFMT_A8R8G8B8;
	PresentParameters.BackBufferCount = 1;
	PresentParameters.MultiSampleType = D3DMULTISAMPLE_4_SAMPLES;
	PresentParameters.MultiSampleQuality = 0;
	PresentParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	PresentParameters.hDeviceWindow = RenderWindow;
	PresentParameters.Windowed = true;
	PresentParameters.EnableAutoDepthStencil = true;
	PresentParameters.AutoDepthStencilFormat = D3DFMT_D24S8;
	PresentParameters.Flags = 0;
	PresentParameters.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	PresentParameters.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	// Create the physical device
	hr = D3D9->CreateDevice(
		D3DADAPTER_DEFAULT,
		DeviceType,
		RenderWindow,
		vp,
		&PresentParameters,
		&Device);

	// Handle if device is not initialized
	if (SUCCEEDED(hr))
	{
		std::cout << "Created device successfully..." << std::endl;

		DeviceCreated = true;
	}
	else if (FAILED(hr))
	{
		D3D9->Release();
		::MessageBoxA(0, "CreateDevice() - FAILED", 0, 0);
		return false;
	}

	D3D9->Release();

	return DeviceCreated;
}

void Renderer::SetRenderState()
{
	Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	//Device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
	//Device->SetRenderState(D3DRS_NORMALIZENORMALS, true);
	//Device->SetRenderState(D3DRS_SPECULARENABLE, false);
	//Device->SetRenderState(D3DRS_AMBIENT, TRUE);
}

bool Renderer::Setup()
{
	D3DXCreateTeapot(
		Device,
		&Teapot,
		0);

	D3DXMatrixTranslation(&TeapotWorldMatrix, 0.0f, 0.0f, 0.0f);

	Device->CreateVertexBuffer(
		8 * sizeof(Vertex),
		D3DUSAGE_WRITEONLY,
		Vertex::FVF,
		D3DPOOL_MANAGED,
		&VertexBuffer,
		0);

	Device->CreateIndexBuffer(
		36 * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&IndexBuffer,
		0);

	Vertex* v;
	VertexBuffer->Lock(0,0,(void**)&v,0);

	// vertices of a unit cube
	v[0] = Vertex(-1.0f, -1.0f, -1.0f);
	v[1] = Vertex(-1.0f, 1.0f, -1.0f);
	v[2] = Vertex(1.0f, 1.0f, -1.0f);
	v[3] = Vertex(1.0f, -1.0f, -1.0f);
	v[4] = Vertex(-1.0f, -1.0f, 1.0f);
	v[5] = Vertex(-1.0f, 1.0f, 1.0f);
	v[6] = Vertex(1.0f, 1.0f, 1.0f);
	v[7] = Vertex(1.0f, -1.0f, 1.0f);
	
	VertexBuffer->Unlock();
	
	//
	// Define the triangles of the box
	//
	WORD* i = 0;
	IndexBuffer->Lock(0,0, (void**)&i,0);

	// fill in the front face index data
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	// fill in the back face index data
	i[6] = 4; i[7] = 6; i[8] = 5;
	i[9] = 4; i[10] = 7; i[11] = 6;

	// fill in the top face index data
	i[12] = 4; i[13] = 5; i[14] = 1;
	i[15] = 4; i[16] = 1; i[17] = 0;

	// fill in the bottom face index data
	i[18] = 3; i[19] = 2; i[20] = 6;
	i[21] = 3; i[22] = 6; i[23] = 7;

	// fill in the left face index data
	i[24] = 1; i[25] = 5; i[26] = 6;
	i[27] = 1; i[28] = 6; i[29] = 2;

	// fill in the right face index data
	i[30] = 4; i[31] = 0; i[32] = 3;
	i[33] = 4; i[34] = 3; i[35] = 7;

	IndexBuffer->Unlock();

	return true;
}

void Renderer::SetupCamera()
{
	position = D3DXVECTOR3(0.0f, 0.0f, -5.0f);
	target = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	D3DXMatrixLookAtLH(
		&view,
		&position,
		&target,
		&up);

	Device->SetTransform(D3DTS_VIEW, &view);

	//
	// Set projection matrix.
	//

	D3DXMatrixPerspectiveFovLH(
		&proj,
		D3DX_PI * 0.25f, // 90 - degree
		(float)Width / (float)Height,
		1.0f,
		1000.0f);

	Device->SetTransform(D3DTS_PROJECTION, &proj);
}

void Renderer::BeginFrame()
{
	if (Device)
	{
		D3DXMATRIX xRot,yRot;
		D3DXMatrixRotationX(&xRot, 3.14f / 4.0f);

		// incremement y-rotation angle each frame
		static float y = 0.0f;
		D3DXMatrixRotationY(&yRot, y);
		y += static_cast<float>(Timer::GetElapsedTime());

		// reset angle to zero when angle reaches 2*PI
		if (y >= 6.28f)
			y = 0.0f;

		// combine x- and y-axis rotation transformations.
		D3DXMATRIX p = xRot * yRot;

		Device->SetTransform(D3DTS_WORLD, &p);

		// Instruct the device to set each pixel on the back buffer with default clear color
		Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x333333, 1.0f, 0);

		// Begin scene rendering
		Device->BeginScene();
	}
}

void Renderer::EndFrame()
{
	// Set the world matrix that positions the object.
	//Device->SetTransform(D3DTS_WORLD, &teapot);

	// Draw the object using the previously set world matrix.
	//Teapot->DrawSubset(0);
	
	Device->SetStreamSource(0, VertexBuffer, 0, sizeof(Vertex));
	Device->SetIndices(IndexBuffer);
	Device->SetFVF(Vertex::FVF);
	Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 8, 0, 12);

	// End rendering scene
	Device->EndScene();
}

void Renderer::RenderFrame()
{
	// Present the buffer to game window
	Device->Present(0, 0, 0, 0);

	std::cout << "Rendering" << std::endl;
}

bool Renderer::Init()
{
	Singleton = new Renderer();

	return Singleton->CreateDevice() && Singleton->PostInit();
}

bool Renderer::PostInit()
{
	Singleton->Setup();
	Singleton->SetupCamera();
	Singleton->SetRenderState();

	return true;
}

void Renderer::Update(float timeDelta)
{
	Singleton->BeginFrame();
	Singleton->EndFrame();
}

void Renderer::Render()
{
	Singleton->RenderFrame();
}
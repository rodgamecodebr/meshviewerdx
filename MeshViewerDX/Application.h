#pragma once

#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <iostream>
#include "Window.h"
#include "Renderer.h"

class Application
{
public:

	Application();
	~Application();

	static bool		 Init();
	static void		 Loop();
	static HINSTANCE GetApplicationWindow();
};

#endif APPLICATION_H_
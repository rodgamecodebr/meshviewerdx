#pragma once

#ifndef RENDERER_H_
#define RENDERER_H_

#include <d3d9.h>
#include <d3dx9.h>
#include <iostream>
#include "Window.h"
#include "MeshViewerUtilities.h"

class Renderer
{
public:
	Renderer();
	~Renderer();

	bool CreateDevice();
	void SetRenderState();
	bool Setup();
	void SetupCamera();
	void BeginFrame();
	void EndFrame();
	void RenderFrame();

	static bool Init();
	static bool PostInit();
	static void Render();
	static void Update(float timeDelta);

private:
	int						  Width;
	int						  Height;
	bool					  DeviceCreated;

	HWND					  RenderWindow;
	_D3DPRESENT_PARAMETERS_   PresentParameters;
	IDirect3D9*               D3D9;
	IDirect3DDevice9*         Device;
	D3DCAPS9                  Caps;
	ID3DXMesh*				  DefaultMesh;
	D3DXMATRIX				  WorldMatrix;
	D3DXVECTOR3				  CameraPosition;
	D3DXVECTOR3				  CameraTargetPosition;
	D3DXVECTOR3				  CameraUpPosition;
	D3DXMATRIX				  CameraView;
	D3DXMATRIX				  CameraProjection;
	IDirect3DVertexBuffer9*   VertexBuffer;
	IDirect3DIndexBuffer9*    IndexBuffer;
	ID3DXMesh*			      Teapot;
	D3DXMATRIX				  TeapotWorldMatrix;
	D3DDEVTYPE				  DeviceType;
	MSG						  Msg;

	static Renderer* Singleton;
};

#endif RENDERER_H_
#include "Timer.h"

LARGE_INTEGER Timer::StartingTime;
LARGE_INTEGER Timer::EndingTime;
LARGE_INTEGER Timer::Frequency;
LARGE_INTEGER Timer::ElapsedMicroseconds;

Timer::Timer()
{
}

Timer::~Timer()
{
}

void Timer::Tick()
{
    QueryPerformanceFrequency(&Frequency);
    QueryPerformanceCounter(&StartingTime);
}

int64_t Timer::GetTicks()
{
    LARGE_INTEGER ticks;
    if (!QueryPerformanceCounter(&ticks))
    {
    }
    return ticks.QuadPart;
}

double Timer::GetElapsedTime()
{
    QueryPerformanceCounter(&EndingTime);
    ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;

    return static_cast<double>((ElapsedMicroseconds.QuadPart * 1000.0) / Frequency.QuadPart);
}
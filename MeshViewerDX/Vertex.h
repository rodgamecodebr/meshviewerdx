#pragma once

#ifndef VERTEX_H_
#define VERTEX_H_

#include <wtypes.h>
#include <d3d9.h>

namespace MeshFVF
{
	struct Vertex
	{
		Vertex() { _x = 0.0f; _y = 0.0f; _z = 0.0f; _nx = .0f; _ny = .0f; _nz = .0f; }
		Vertex(float x, float y, float z,
			float nx, float ny, float nz, float u, float v)
		{
			_x = x;   _y = y;   _z = z;
			_nx = nx; _ny = ny; _nz = nz;
			_u = u;   _v = v;
		}

		float _x, _y, _z, _nx, _ny, _nz, _u, _v;

		static const DWORD FVF;
	};
	const DWORD Vertex::FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;
}

#endif VERTEX_H_
#pragma once

#ifndef GAMEWORLD_H_
#define GAMEWORLD_H_

#include <iostream>

class GameWorld
{
public:
	GameWorld();
	~GameWorld();

	void GameLoop(float timeDelta);
};

#endif GAMEWORLD_H_

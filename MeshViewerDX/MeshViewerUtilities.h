#pragma once

#ifndef MESHVIEWERUTILITIES_H_
#define MESHVIEWERUTILITIES_H_

namespace MeshViewerUtilities
{
	template<class T> void Release(T t)
	{
		if (t)
		{
			t->Release();
			t = 0;
		}
	}

	template<class T> void Delete(T t)
	{
		if (t)
		{
			delete t;
			t = 0;
		}
	}
}

#endif MESHVIEWERUTILITIES_H_
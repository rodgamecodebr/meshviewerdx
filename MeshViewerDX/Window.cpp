#include "Window.h"

static Window* Singleton = 0;

Window::Window()
{
    hInst = (HINSTANCE)GetModuleHandle(NULL);
    hWnd = NULL;
    szWindowClass = "Window";
    szTitle = "Mesh Viewer Direct3D9";
    WndProc = WindowProc;
    created = false;
    cmdShow = 1;
}

Window::Window(LPCSTR title)
{
    hInst = (HINSTANCE)GetModuleHandle(NULL);
    hWnd = NULL;
    szWindowClass = "Window";
    WndProc = WindowProc;
    created = false;
    cmdShow = 1;

    szTitle = title;
}

Window::~Window()
{
}

int Window::Create()
{
    WNDCLASSEX wcex = { 0 };

    wcex.cbSize = sizeof(wcex);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.hInstance = hInst;
    wcex.lpszClassName = szWindowClass;
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

    if (!RegisterClassEx(&wcex))
    {
        MessageBox(NULL,
            _T("Call to RegisterClassEx failed!"),
            _T("Windows Desktop Guided Tour"),
            NULL);

        return 1;
    }

    hWnd = CreateWindowEx(
        WS_EX_OVERLAPPEDWINDOW,
        szWindowClass,
        szTitle,
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        1440, 900,
        NULL,
        NULL,
        hInst,
        NULL
    );

    if (!hWnd)
    {
        MessageBox(NULL,
            _T("Call to CreateWindow failed!"),
            _T("Windows Desktop Guided Tour"),
            NULL);

        return 1;
    }

    // The parameters to ShowWindow explained:
    // hWnd: the value returned from CreateWindow
    // nCmdShow: the fourth parameter from WinMain
    ShowWindow(hWnd,
        cmdShow);
    UpdateWindow(hWnd);

    created = true;

    return created;
}

void Window::Update()
{
    // Main message loop:
     MSG msg;

    ::ZeroMemory(&msg, sizeof(MSG));

    while (msg.message != WM_QUIT)
    {
        if (::PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            // Get Current Time
            Timer::Tick();

            // Update Loop goes here
            gameWorld->GameLoop(Timer::GetElapsedTime());
            std::cout << Timer::GetElapsedTime() << std::endl;
            
            Timer::EndingTime = Timer::StartingTime;
        }
    }
}

bool Window::Init()
{
    Singleton = new Window();

    return GetInstance()->Create();
}

bool Window::Loop()
{
    GetInstance()->Update();

    return true;
}

Window* Window::GetInstance()
{
    return Singleton;
}

HINSTANCE Window::GetHInstance()
{
    return Singleton->hInst;
}

HWND Window::GetHwnd()
{
    return Singleton->hWnd;
}

LRESULT Window::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_KEYDOWN:
        if (wParam == VK_ESCAPE)
            DestroyWindow(hwnd);
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
        break;
    }

    return 0;
}
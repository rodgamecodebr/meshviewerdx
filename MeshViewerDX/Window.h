#pragma once

#ifndef WINDOW_H_
#define WINDOW_H_

#include <tchar.h>
#include <memory>
#include "Timer.h"
#include "GameWorld.h"

class Window
{
public:
	Window();
	Window(LPCSTR title);
	~Window();

	int Create();
	void Update();

	static bool Init();
	static bool Loop();

	static Window* GetInstance();
	static HINSTANCE GetHInstance();
	static HWND GetHwnd();

private:

	int   cmdShow;
	float timeDelta;

	bool  created;

	LPCSTR szWindowClass;
	LPCSTR szTitle;
	WNDPROC WndProc;
	HINSTANCE hInst;
	HWND hWnd;

	GameWorld* gameWorld;

	Window* Instance;
	//Timer* timer;

	// windows callback event handler 
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
};

#endif WINDOW_H_
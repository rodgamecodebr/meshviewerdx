#pragma once

#ifndef TIMER_H_
#define TIMER_H_

#include <Windows.h>
#include <timeapi.h>
#include <profileapi.h>
#include <cstdint>
#include <chrono>

class Timer
{
public:
	Timer();
	~Timer();

	static void Tick();
	static int64_t GetTicks();
	static double GetElapsedTime();

	static LARGE_INTEGER StartingTime, EndingTime, ElapsedMicroseconds;
	static LARGE_INTEGER Frequency;
};

#endif TIMER_H_
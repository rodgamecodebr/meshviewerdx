#include "GameWorld.h"
#include "Renderer.h"

GameWorld::GameWorld()
{
}

GameWorld::~GameWorld()
{
}

void GameWorld::GameLoop(float timeDelta)
{
	Renderer::Update(timeDelta);

	Renderer::Render();
}
